const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ReactRefreshPlugin = require('@pmmmwh/react-refresh-webpack-plugin');

const isDevelopment = env => env === 'development';

const getHotDevClient = (env) => {
  return isDevelopment(env) && require.resolve("react-dev-utils/webpackHotDevClient");
}

const getEntry = (element, env) => [
  getHotDevClient(env),
  `./src/elements/${element}.tsx`,
].filter(Boolean);

const getEntries = (env) => {
  const entries = {
    popup: getEntry('Popup', env),
    options: getEntry('Options', env),
  };

  if (isDevelopment(env)) {
    entries.main = getEntry('Options', env)
  }
  return entries;
}

const getPlugins = (env, paths) => isDevelopment(env) ? [
  new HtmlWebpackPlugin({
    inject: true,
    chunks: ["main"],
    template: paths.appHtml,
  }),
  new ReactRefreshPlugin()
] : [
  new HtmlWebpackPlugin({
    inject: true,
    chunks: ["popup"],
    template: paths.appHtml,
    filename: 'popup.html',
  }),
  new HtmlWebpackPlugin({
    inject: true,
    chunks: ["options"],
    template: paths.appHtml,
    filename: 'options.html',
  }),
]

module.exports = {
  webpack: {
    configure: (webpackConfig, { env, paths }) => {
      return {
        ...webpackConfig,
        entry: getEntries(env),
        output: {
          ...webpackConfig.output,
          filename: "static/js/[name].js",
        },
        optimization: {
          ...webpackConfig.optimization,
          runtimeChunk: false,
        },
        plugins: [
          new MiniCssExtractPlugin(),
          ...getPlugins(env, paths),
        ].filter(Boolean)
      };
    },
  },
};
