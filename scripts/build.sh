#!/bin/bash

build() {
    echo 'Building Site Blocker'

    rm -rf dist/*
    rm -rf build/*

    INLINE_RUNTIME_CHUNK=false GENERATE_SOURCEMAP=false craco build

    mkdir -p dist
    cp -r build/* dist
}

build